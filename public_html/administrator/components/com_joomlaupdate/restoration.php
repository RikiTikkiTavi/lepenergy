<?php
defined('_AKEEBA_RESTORATION') or die('Restricted access');
$restoration_setup = array(
	'kickstart.security.password' => 'Y6HWWp7YZUCXKdtIqL0zn4quyqkdykWz',
	'kickstart.tuning.max_exec_time' => '5',
	'kickstart.tuning.run_time_bias' => '75',
	'kickstart.tuning.min_exec_time' => '0',
	'kickstart.procengine' => 'direct',
	'kickstart.setup.sourcefile' => 'D:\OpenServer\OpenServer\domains\lepenergy.lv/tmp/Joomla_3.7.3-Stable-Update_Package.zip',
	'kickstart.setup.destdir' => '/var/www/lepenergy.lv/public_html',
	'kickstart.setup.restoreperms' => '0',
	'kickstart.setup.filetype' => 'zip',
	'kickstart.setup.dryrun' => '0');