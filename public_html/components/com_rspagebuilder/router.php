<?php
/**
 * @package RSPageBuilder!
 * @copyright (C) 2016 www.rsjoomla.com
 * @license GPL, http://www.gnu.org/licenses/gpl-3.0.html
 */

// No direct access
defined ('_JEXEC') or die ('Restricted access');

class RSPageBuilderRouter extends JComponentRouterBase {
	
	public function build(&$query) {
		$segments	= array();

		$app		= JFactory::getApplication();
		$menu		= $app->getMenu();
		$params		= JComponentHelper::getParams('com_rspagebuilder');
		$advanced	= $params->get('sef_advanced_link', 0);

		if (empty($query['Itemid'])) {
			$menuItem = $menu->getActive();
			$menuItemGiven = false;
		} else {
			$menuItem = $menu->getItem($query['Itemid']);
			$menuItemGiven = true;
		}

		// Check again
		if ($menuItemGiven && isset($menuItem) && $menuItem->component != 'com_rspagebuilder') {
			$menuItemGiven = false;
			unset($query['Itemid']);
		}

		if (isset($query['layout'])) {
			unset($query['layout']);
		}

		if (isset($query['view'])) {
			$view = $query['view'];
		} else {
			return $segments;
		}

		if (($menuItem instanceof stdClass) && $menuItem->query['view'] == $query['view'] && isset($query['id']) && $menuItem->query['id'] == (int) $query['id']) {
			unset($query['view']);
			unset($query['id']);

			return $segments;
		}

		if ($view == 'page') {
			if (!$menuItemGiven) {
				$segments[] = $view;
			}

			unset($query['view']);

			if ($view == 'page') {
				if (isset($query['id'])) {
					if (strpos($query['id'], '-') === false) {
						$db = JFactory::getDbo();
						$dbQuery = $db->getQuery(true)
							->select('alias')
							->from('#__rspagebuilder')
							->where('id=' . (int) $query['id']);
						$db->setQuery($dbQuery);
						$alias = $db->loadResult();
						$query['id'] = $query['id'] . '-' . $alias;
					}
				} else {
					return $segments;
				}
			} else {
				return $segments;
			}

			$id			= $query['id'];
			$segments[]	= $id;
			unset($query['id']);
		}
		
		if ($view == 'pages') {
			$segments[] = $view;
			
			if (isset($query['view'])) {
				unset($query['view']);
			}
			if (isset($query['Itemid'])) {
				unset($query['Itemid']);
			}
		}

		return $segments;
	}

	public function parse(&$segments) {
		$total	= count($segments);
		$vars	= array();

		for ($i = 0; $i < $total; $i++) {
			$segments[$i] = preg_replace('/-/', '-', $segments[$i], 1);
		}

		// Get the active menu item.
		$app		= JFactory::getApplication();
		$menu		= $app->getMenu();
		$item		= $menu->getActive();
		$params 	= JComponentHelper::getParams('com_rspagebuilder');
		$advanced	= $params->get('sef_advanced_link', 0);
		$db 		= JFactory::getDbo();

		// Count route segments
		$count = count($segments);

		if (!isset($item)) {
			$vars['view']	= $segments[0];
			$vars['id']		= $segments[$count - 1];

			return $vars;
		}

		if ($count == 1) {
			if (strpos($segments[0], '-') === false) {
				$vars['view']	= 'page';
				$vars['id']		= (int) $segments[0];

				return $vars;
			}

			list($id, $alias) = explode('-', $segments[0], 2);
		}

		if (!$advanced) {
			$article_id = (int) $segments[$count - 1];

			if ($article_id > 0) {
				$vars['view']	= 'page';
				$vars['id']		= $article_id;
			}

			return $vars;
		}

		return $vars;
	}
}


function RSPageBuilderBuildRoute(&$query) {
	$router = new RSPageBuilderRouter;

	return $router->build($query);
}

function RSPageBuilderParseRoute($segments) {
	$router = new RSPageBuilderRouter;

	return $router->parse($segments);
}