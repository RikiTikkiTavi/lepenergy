<?php
/**
 * @package RSPageBuilder!
 * @copyright (C) 2016 www.rsjoomla.com
 * @license GPL, http://www.gnu.org/licenses/gpl-3.0.html
 */

// No direct access
defined ('_JEXEC') or die ('Restricted access');
?>

<!--PAGE HEADING-->

<?php if ($this->show_page_heading) { ?>

<section id="pageHeading">

    <div class="container">
        <div class="row">
            <div class="col-7 text-left">
                <h3>
                    <?php
                    if ($this->page_heading) {
                        echo $this->escape($this->page_heading);
                    } else {
                        echo $this->page->title;
                    }
                    ?>
                </h3>
            </div>
            <div class="col-5 text-right">
                <span><a href="/">Lepenergy.lv</a> /
                    <?php
                    if ($this->page_heading) {
                        echo $this->escape($this->page_heading);
                    } else {
                        echo $this->page->title;
                    }
                    ?>
                </span>
            </div>
        </div>
    </div>

</section>
<?php } ?>
<!--/PAGE HEADING-->

<!--PAGE CONTENT-->

<section id="pageContent">
    <div class="container">
        <?php echo ElementParser::viewPage(json_decode($this->page->content), $this->page->bootstrap_version, $this->page->full_width, true); ?>
    </div>
</section>

<!--/PAGE CONTENT-->