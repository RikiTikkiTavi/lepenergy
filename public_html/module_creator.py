import os

module_name = raw_input("Enter module name: ")

if os.path.isdir("./tmp"):
    if not os.path.exists("./tmp/"+module_name):
        #  Paths
        os.mkdir("./tmp/" + module_name)
        os.mkdir("./tmp/" + module_name + "/tmpl")
        #  Files ./:
        os.mknod("./tmp/" + module_name + "/" + module_name + ".php")  # module_name.php
        os.mknod("./tmp/" + module_name + "/" + module_name + ".xml")  # module_name.xml
	os.mknod("./tmp/" + module_name + "/helper.php")  # helper.php
	#  Files ./tmpl
        os.mknod("./tmp/" + module_name + "/index.html")  # index.html
        os.mknod("./tmp/" + module_name + "/tmpl/default.php")  # /tmpl/default.php
        os.mknod("./tmp/" + module_name + "/tmpl/index.html")  # /tmpl/index.html
        print "Module <" + module_name + "> created";
    else:
        print "MISTAKE! Module with name <" + module_name + "> exists"
else:
    print "MISTAKE! No ./tmp dir"
