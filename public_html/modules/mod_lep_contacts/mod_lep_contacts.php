<?php
/**
 * LEP_CONTACTS entry point
 *
 * @package Joomla.Site
 * @subpackage Modules
 * @license Payment
 * @copyright Egor
 * @since 07.05.2017
 */

defined('_JEXEC') or die;

require_once dirname(__FILE__) . '/helper.php';

if (isset($params)) {
    $tel = $params ->get('tel', 1);
    $email = $params ->get('mail', 1);
    $address = $params ->get('address', 1);
    $hours = $params ->get('hours', 1);
    $fb = $params ->get('fb', 1);
    $tw = $params ->get('tw', 1);
    $map = $params ->get('map', 1);
}


require JModuleHelper::getLayoutPath('mod_lep_contacts');
