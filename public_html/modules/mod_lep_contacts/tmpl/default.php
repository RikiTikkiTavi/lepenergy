<?php
// No direct access
defined('_JEXEC') or die;
?>

<!--CONTACTS-->
<section id="contacts">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="section-heading text-center"><?=$module -> title ?></h3>
                <hr style="border-color:#030066;">
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-8">
                <?=$map?>
            </div>
            <div class="col-md-4">
                <p><?=$address?></p>
                <p><i class="fa fa-phone"></i> <abbr title="Phone">P</abbr>: <?=$tel?> </p>
                <p><i class="fa fa-envelope-o"></i> <abbr title="Email">E</abbr>: <a href="mailto:<?=$email?>"><?=$email?></a>
                </p>
                <p><i class="fa fa-clock-o"></i> <abbr title="Hours">H</abbr>: <?=$hours?></p>
                <p><a class="social-icon" href="<?=$fb?>"> <i class="fa fa-facebook-official" aria-hidden="true"></i></a> <a
                            class="social-icon" href="<?=$tw?>"> <i class="fa fa-twitter-square" aria-hidden="true"></i></a></p>
            </div>
        </div>
    </div>
</section>
<!--/CONTACTS-->