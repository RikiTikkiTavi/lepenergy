<?php
/**
 * LEP_HEADSLIDER entry point
 *
 * @package Joomla.Site
 * @subpackage Modules
 * @license Payment
 * @copyright Egor Paromov
 * @since 07.05.2017
 */

// No direct access
defined('_JEXEC') or die;

// Include the syndicate functions only once
require_once dirname(__FILE__) . '/helper.php';

if (isset($params)) {
// 1. Slide
    $slide1_img = $params->get('slide1_img', 1);
    $slide1_h = $params->get('slide1_h', 1);
    $slide1_p = $params->get('slide1_p', 1);
    $slide1_button = $params->get('slide1_button', 1);


// 2. Slide
    $slide2_img = $params->get('slide2_img', 2);
    $slide2_h = $params->get('slide2_h', 2);
    $slide2_p = $params->get('slide2_p', 2);
    $slide2_button = $params->get('slide2_button', 2);

// 3. Slide
    $slide3_img = $params->get('slide3_img', 3);
    $slide3_h = $params->get('slide3_h', 3);
    $slide3_p = $params->get('slide3_p', 3);
    $slide3_button = $params->get('slide3_button', 3);

}

require JModuleHelper::getLayoutPath('mod_lep_headslider');