CREATE TABLE IF NOT EXISTS `#__lep_headslider` (
  `slide_id`     INT(10)     NOT NULL AUTO_INCREMENT,
  `slide_img`    TEXT        NOT NULL,
  `slide_h`      TEXT        NOT NULL,
  `slide_p`      TEXT        NOT NULL,
  `slide_button` TEXT        NOT NULL,
  `lang`         VARCHAR(25) NOT NULL,

  PRIMARY KEY (`slide_id`)
)
  ENGINE = INNODB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

INSERT INTO `#__lep_headslider` (`slide_img`, `slide_h`, `slide_p`, `slide_button`, `lang`)
VALUES ('<img src="">', 'Welcome to lepenergy', 'Slide p', 'Learn more', 'en-GB');