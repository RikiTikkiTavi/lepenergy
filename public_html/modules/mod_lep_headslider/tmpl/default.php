<?php
// No direct access
defined('_JEXEC') or die; ?>

<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1" class=""></li>
        <li data-target="#myCarousel" data-slide-to="2" class=""></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        <div class="carousel-item active">
            <img class="first-slide"
                 src="/<?= $slide1_img ?>">
            <div class="container-slider">
                <div class="align-items-center carousel-caption">
                    <h1><?= $slide1_h ?></h1><br>
                    <p><?= $slide1_p ?></p><br>
                    <p><a class="btn btn-lg btn-warning page-scroll" href="#services"
                          role="button"><?= $slide1_button ?></a></p>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <img class="second-slide"
                 src="/<?= $slide2_img ?>">
            <div class="container-slider">
                <div class="carousel-caption text-left">
                    <h1><?= $slide2_h ?></h1>
                    <p><?= $slide2_p ?></p>
                    <p><a class="btn btn-lg btn-success page-scroll" href="#contacts"
                          role="button"><?= $slide2_button ?></a></p>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <img class="third-slide" src="/<?= $slide3_img ?>">
            <div class="container-slider">
                <div class="carousel-caption">
                    <h1><?= $slide3_h ?></h1>
                    <p><?= $slide3_p ?></p>
                    <p><a class="btn btn-lg btn-primary page-scroll" href="#portfolio"
                          role="button"><?= $slide3_button ?></a></p>
                </div>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>