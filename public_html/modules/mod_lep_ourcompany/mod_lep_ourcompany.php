<?php
/**
 * LEP_OURCOMPANY entry point
 *
 * @package Joomla.Site
 * @subpackage Modules
 * @license Payment
 * @copyright Egor
 * @since 07.05.2017
 */

defined('_JEXEC') or die;

require_once dirname(__FILE__) . '/helper.php';

//Services arr
if (isset($params)) {
    $oc_text = $params ->get('oc_text', 1);
    $oc_image1 = $params ->get('oc_image1', 1);
    $oc_image2 = $params ->get('oc_image2', 1);
    $oc_image3 = $params ->get('oc_image3', 1);
    $oc_button_text = $params ->get('oc_button_text', 1);
    $oc_button_link = $params ->get('oc_button_link', 1);
}


require JModuleHelper::getLayoutPath('mod_lep_ourcompany');
