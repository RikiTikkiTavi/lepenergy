<?php
// No direct access
defined('_JEXEC') or die;
?>

<?php if (isset($params) && isset($module)) { ?>

    <!--ABOUT US & WHY US-->
    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="section-heading text-center"><?= $module->title ?></h3>
                            <hr style="border-color:#279d02;">
                        </div>
                    </div>
                    <div class="row text-center">
                        <div class="col-12">
                            <p>
                                <?= $oc_text ?>
                            </p>
                            <div class="row">
                                <div class="col">
                                    <a class="image-popup-no-margins" href="/<?= $oc_image1 ?>"><img class="img-fluid"
                                                                                                     src="/<?= $oc_image1 ?>"></a>
                                </div>
                                <div class="col">
                                    <a class="image-popup-no-margins" href="/<?= $oc_image2 ?>"><img class="img-fluid"
                                                                                                     src="/<?= $oc_image2 ?>"></a>
                                </div>
                                <div class="col">
                                    <a class="image-popup-no-margins" href="/<?= $oc_image3 ?>"><img class="img-fluid"
                                                                                                     src="/<?= $oc_image3 ?>"></a>
                                </div>
                            </div>
                            <br>
                            <a class="btn btn-md btn-success" href="<?= JRoute::_('index.php?Itemid=' . $oc_button_link) ?>"
                               role="button"><?= $oc_button_text ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/ABOUT US & WHY US-->

<?php } ?>
