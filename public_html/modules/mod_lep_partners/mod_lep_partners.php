<?php
/**
 * LEP_PORTFOLIO entry point
 *
 * @package Joomla.Site
 * @subpackage Modules
 * @license Payment
 * @copyright Egor
 * @since 07.05.2017
 */

defined('_JEXEC') or die;

require_once dirname(__FILE__) . '/helper.php';

//Services arr
if (isset($params)) {
    $partners = $params ->get('partners', 1);
    $p_button_text = $params ->get('p_button_text', 1);
    $p_button_link = $params ->get('p_button_link', 1);
}


require JModuleHelper::getLayoutPath('mod_lep_partners');
