<?php
// No direct access
defined('_JEXEC') or die;
?>

<!--PARTNERS-->
<section id="partners">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!--<div class="row">
                    <div class="col-md-12">
                        <h3 class="section-heading text-center"><?/*= $module->title */?></h3>
                        <hr style="border-color:#ffbc13;">
                    </div>
                </div>-->
                <!--<br><br>-->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="owl-carousel owl-theme">
                                <?php
                                if (isset($params)) {
                                    foreach ($partners as $partner) {
                                        ?>
                                        <div class="item text-center"><img src="/<?= $partner->p_logo ?>"
                                                                               alt="<?= $partner->p_name ?>"></div>
                                    <?php }
                                } ?>
                            </div>
                        </div>

                    </div>
                </div>
                <!--<br><br>
                <div class="row text-center">
                    <div class="col-md-12">
                        <a class="btn btn-md btn-warning" href="<?/*=$p_button_link*/?>" role="button"><?/*=$p_button_text*/?></a>
                    </div>
                </div>-->
            </div>
        </div>
    </div>
</section>
<!--/PARTNERS-->