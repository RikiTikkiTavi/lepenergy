<?php
/**
 * LEP_PORTFOLIO entry point
 *
 * @package Joomla.Site
 * @subpackage Modules
 * @license Payment
 * @copyright Egor
 * @since 07.05.2017
 */

defined('_JEXEC') or die;

require_once dirname(__FILE__) . '/helper.php';

//Services arr
if (isset($params)) {
    $items = (array) $params ->get('portfolio_items', 1);
    $items = array_reverse($items);
}


require JModuleHelper::getLayoutPath('mod_lep_portfolio');
