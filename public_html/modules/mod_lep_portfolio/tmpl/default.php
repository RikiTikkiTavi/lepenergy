<?php
// No direct access
defined('_JEXEC') or die;
?>

<!--PORTFOLIO-->
<section id="portfolio">
    <div class="container-fluid">

        <!--HEADING ROW-->
        <div class="row">
            <div class="col-12">
                <h3 class="section-heading text-center"><?= $module->title ?></h3>
                <hr style="border-color:#ffbc13;">
            </div>
        </div>
        <!--/HEADING ROW-->

        <!--PORTFOLIO CONTENT ROW-->
        <div class="row content-row">
            <div class="row no-gutter popup-gallery">


                <!--PORTFOLIO ITEMS-->
                <?php
                if (isset($items)) {
                    $i=0;
                    foreach ($items as $item) {
                        ?>

                        <!--ITEM-->
                        <div class="col-md-3 col-6">
                            <a href="/<?= $item->p_project_image ?>" class="portfolio-box">
                                <img src="/<?= $item->p_project_image ?>" class="img-fluid" alt="">
                                <div class="portfolio-box-caption">
                                    <div class="portfolio-box-caption-content">
                                        <div class="project-category text-faded">
                                            <?= $item->p_service_name ?>
                                        </div>
                                        <div class="project-name">
                                            <?= $item->p_project_name ?>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!--/ITEM-->

                        <?php
                        $i++;
                        if ($i == 8) {
                            break;
                        }
                    }
                } ?>
                <!--/PORTFOLIO ITEMS-->


            </div>
        </div>
        <!--/PORTFOLIO CONTENT ROW-->

    </div>
</section>
<!--/PORTFOLIO-->