<?php
/**
 * LEP_SERVICES entry point
 *
 * @package Joomla.Site
 * @subpackage Modules
 * @license Payment
 * @copyright Egor
 * @since 07.05.2017
 */

defined('_JEXEC') or die;

require_once dirname(__FILE__) . '/helper.php';

//Services arr
if (isset($params)) {
    $services = $params ->get('services_items', 1);
    $text = $params -> get('after_s_t',1);
    $b_text = $params -> get('after_s_b_t',1);
    $b_link = $params -> get('after_s_b_l',1);
    $show = $params -> get('show_after_services_section', "");
}


require JModuleHelper::getLayoutPath('mod_lep_services');
