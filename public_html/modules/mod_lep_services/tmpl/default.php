<?php
// No direct access
defined('_JEXEC') or die;
?>

<!--SECTION SERVICES-->
<section id="services">

    <div class="container">

        <!--HEADING ROW-->
        <div class="row">
            <div class="col-md-12">
                <h3 class="section-heading text-center"><?= $module->title ?></h3>
                <hr style="border-color:#030066;">
            </div>
        </div>
        <!--/HEADING ROW-->

        <div class="row">
            <?php
            if (isset($services)) {
                foreach ($services as $service) {
                    ?>
                    <!--SERVICES NEW-->
                    <div class="col-md-4 col-sm-6 col-12">
                        <div class="card">
                            <img class="card-img-top img-fluid" src="/<?= $service->s_img ?>" alt="Card image cap">
                            <div class="card-block">
                                <p class="card-title"><?= $service->s_h ?></a></p>
                                <p class="card-text"><?= $service->s_d?></p>
                            </div>
                        </div>
                    </div>
                    <!--/SERVICES NEW-->
                <?php }
            } ?>
        </div>

    </div>
</section>
<!--/SECTION SERVICES-->

<!--AFTER SERVICES-->
<?php if ($show = "1") { ?>
    <section id="after-services">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-6 col-12 align-self-center text-center">
                    <h4 style="margin-top:10px"><?= $text ?></h4>
                </div>
                <div class="col-md-6 col-lg-6 col-12 text-center">
                    <a class="btn btn-warning btn-more-orange" href="<?= JRoute::_('index.php?Itemid=' . $b_link) ?>"><?= $b_text ?></a>
                </div>
                <!--<div class="col-12 text-center">
                    <h4 style="margin-top:10px"><?/*= $text */?></h4>
                    <a class="btn btn-warning btn-more-orange" href="<?/*= JRoute::_('index.php?Itemid=' . $b_link) */?>"><?/*= $b_text */?></a>
                </div>-->
            </div>
        </div>
    </section>
<?php } ?>
<!--/AFTER SERVICES-->
