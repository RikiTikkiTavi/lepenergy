<?php
/**
 * LEP_TOPCONTACTBAR entry point
 *
 * @package Joomla.Site
 * @subpackage Modules
 * @license Payment
 * @copyright Egor Paromov
 * @since 07.05.2017
 */

// No direct access
defined('_JEXEC') or die;

// Include the syndicate functions only once
require_once dirname(__FILE__) . '/helper.php';

$app  = JFactory::getApplication();
$menu = $app->getMenu();
$lang = JFactory::getLanguage();
$is_home_page = $menu->getActive() == $menu->getDefault( $lang->getTag() );

// 1. Slide
$facebook_link = $params->get('facebook_link', "/");
$twitter_link = $params->get('twitter_link', "/");
$tel = $params->get('tel', "");
$email = $params->get('email', "");
$work_hours = $params->get('work_hours', "");

require JModuleHelper::getLayoutPath('mod_lep_topcontactbar');