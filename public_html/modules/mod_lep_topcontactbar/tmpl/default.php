<?php
// No direct access
defined('_JEXEC') or die; ?>

<?php if ($is_home_page){ ?>
<nav id="topcontactnavbar" class="navbar fixed-top d-none d-md-block">
    <?php } else { ?>
    <nav id="topcontactnavbarPage" class="navbar d-none d-md-block">
        <?php } ?>
        <div class="container">
            <div class="row">
                <ul class="nav social">
                    <li><a href="<?= $facebook_link ?>"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                    </li>
                    <li><a href="<?= $twitter_link ?>"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                </ul>
                <ul class="nav ml-auto small-contact">
                    <li><i class="fa fa-phone-square" aria-hidden="true"></i> <?= $tel ?></li>
                    <li><i class="fa fa-envelope" aria-hidden="true"></i> <?= $email ?></li>
                    <li><i class="fa fa-clock-o" aria-hidden="true"></i> <?= $work_hours ?></li>
                </ul>
            </div>
        </div>
    </nav>