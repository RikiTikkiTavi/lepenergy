<?php
defined('_JEXEC') or die; ?>

<header>
    <jdoc:include type="modules" name="header1"/>
    <nav id="topmainnavbar" class="navbar fixed-top navbar-toggleable-md navbar-inverse attached">
        <div class="container">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                    data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false"
                    aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand page-scroll" href="#page-top">
                <img src="<?=$template_url?>/img/logo.png" width="140px" class="d-inline-block align-top" alt="">
            </a>

            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <jdoc:include type="modules" name="header2"/>
                <jdoc:include type="modules" name="header2_lang_switcher"/>
            </div>
        </div>
    </nav>
    <jdoc:include type="modules" name="header3"/>
</header>


<!--SERVICES-->
<jdoc:include type="modules" name="section#1"/>
<!--/SERVICES-->

<!--PORTFOLIO-->
<jdoc:include type="modules" name="section#2"/>
<!--/PORTFOLIO-->

<!--OUR COMPANY-->
<jdoc:include type="modules" name="section#3"/>
<!--/OUR COMPANY-->

<!--PARTNERS-->
<jdoc:include type="modules" name="section#4"/>
<!--/PARTNERS-->

<!--CONTACTS-->
<jdoc:include type="modules" name="section#5"/>
<!--/CONTACTS-->
