<?php
defined('_JEXEC') or die;
$app  = JFactory::getApplication();
$doc  = JFactory::getDocument();
$menu = $app->getMenu();
$lang = JFactory::getLanguage();

$template_url = $this->baseurl . '/templates/' . $this->template;

$is_home_page = $menu->getActive() == $menu->getDefault($lang->getTag());


?>


<!DOCTYPE html>
<html lang="en" xmlns:jdoc="http://www.w3.org/1999/XSL/Transform">
<head>
	<?php
	$headlink = $this->getHeadData();
	unset($headlink['scripts']['/media/system/js/caption.js']);
	unset($headlink['scripts']['/media/system/js/validate.js']);
	unset($headlink['scripts']['/templates/protostar/js/template.js']);
	unset($headlink['scripts']['/media/jui/js/jquery.min.js']);
	unset($headlink['scripts']['/media/jui/js/jquery-migrate.min.js']);
	unset($headlink['scripts']['/media/jui/js/bootstrap.min.js']);
	unset($headlink['scripts']['/media/jui/js/jquery-noconflict.js']);
	$this->setHeadData($headlink);
	?>

    <jdoc:include type="head"/>


    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="icon" type="image/png"
          href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/img/favicon.png"/>

    <link rel="stylesheet"
          href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/bootstrap.min.css"
          type="text/css"/>
	<?php if ($is_home_page)
	{
		echo "<link rel='stylesheet' href='$this->baseurl/templates/$this->template/css/lepenergy.css'
          type='text/css'/>";
	}
	else
	{
		echo "<link rel='stylesheet' href='$this->baseurl/templates/$this->template/css/page.css'
          type='text/css'/>";
	}
	?>
    <link rel="stylesheet"
          href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/font-awesome/css/font-awesome.min.css"
          type="text/css"/>
    <link rel="stylesheet"
          href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/magnific-popup/magnific-popup.css"/>
    <link rel="stylesheet"
          href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/owl-carousel/assets/owl.carousel.min.css"/>
</head>
<body id="page-top">

<div id="preloader">
    <div class="container">
        <div class="row text-center justify-content-center">
            <div class="col-12">
                <i class="fa fa-spinner fa-spin fa-2x"></i>
            </div>
        </div>
    </div>
</div>

<?php
if ($is_home_page)
{
	include('home.php');
}
else
{
	include('page.php');
}
?>

<footer>
    <p style="color: white;">
        <jdoc:include type="modules" name="footer"/>
    </p>
</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="<?= $template_url ?>/js/jquery.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="<?= $template_url ?>/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= $template_url ?>/js/scrollreveal.min.js" type="text/javascript"></script>
<script src="<?= $template_url ?>/magnific-popup/jquery.magnific-popup.min.js" type="text/javascript"></script>
<script src="<?= $template_url ?>/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>

<!-- Custom js -->
<script src="<?= $template_url ?>/js/lepenergy.js" type="text/javascript"></script>

</body>

</html>
