/**
 * Created by Egor on 6/4/2017.
 */

(function ($) {

    $(window).on('load', function () {
        var $preloader = $('#preloader'),
            $spinner   = $preloader.find('.fa-spinner');
        $spinner.fadeOut();
        $preloader.delay(350).fadeOut('slow');
    });

    $('#myCarousel').carousel({
        interval: 5000
    });

    // jQuery for page scrolling feature - requires jQuery Easing plugin
    $(document).on('click', 'a.page-scroll', function (event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 50)
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
    });

    $(window).on('scroll', function (event) {
        var scrollValue = $(window).scrollTop();
        if ($("section#services").length ) {
            var servicesOffset = $("section#services").offset().top;
            if (scrollValue > servicesOffset - 60) {
                $('#topmainnavbar').removeClass("attached").addClass('detached');
                $('.navbar-brand img').attr("width", "120px")
            }
            if (scrollValue < servicesOffset - 60) {
                $('#topmainnavbar').removeClass('detached').addClass("attached");
                $('.navbar-brand img').attr("width", "140px")
            }
        }
    });

    !function (a) {
        "use strict";
        a(".popup-gallery").magnificPopup({
            delegate: "a",
            type: "image",
            tLoading: "Loading image #%curr%...",
            mainClass: "mfp-img-mobile",
            gallery: {
                enabled: !0,
                navigateByImgClick: !0,
                preload: [0, 1]
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
            },
            zoom: {
                enabled: true,
                duration: 300
            }
        });

        $('.image-popup-no-margins').magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            closeBtnInside: false,
            fixedContentPos: true,
            mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
            image: {
                verticalFit: true
            },
            zoom: {
                enabled: true,
                duration: 300 // don't foget to change the duration also in CSS
            }
        });
    }(jQuery);


    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        navText: false,
        pagination: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 5
            }
        }
    });

})(jQuery);


