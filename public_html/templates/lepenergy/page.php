<?php
defined('_JEXEC') or die; ?>

<?php
$input = JFactory::getApplication()->input;
$id = $input->getInt('id'); //get the article ID
$article = JTable::getInstance('content');
$article->load($id);
?>

<header>
    <jdoc:include type="modules" name="header1"/>
    <nav id="topmainnavbarPage" class="navbar navbar-toggleable-md navbar-light attached">
        <div class="container">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                    data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false"
                    aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand page-scroll" href="#page-top">
                <img src="<?=$template_url?>/img/logo.png" width="140px" class="d-inline-block align-top" alt="">
            </a>

            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <jdoc:include type="modules" name="header2"/>
                <jdoc:include type="modules" name="header2_lang_switcher"/>
            </div>
        </div>
    </nav>
</header>

<jdoc:include type="component"/>
